# facebook_messages_analyzer
Provide some basic statistics about Facebook (Messenger) conversation e.g. most common words, who send more messages, messages count etc.

Some info:

It's possible to download copy of data Facebook stores about you. I use Facebook Messenger to chat with my girlfriend and I was interested who sen more messages in total. So I downloaded data from Facebook in JSON file (almost 2 gigs...) and had some fun with it.

IMPORTANT: Encoding is completely broken right now, it will be fixed in one of upcoming updates.

General usage:

First you have to request a download of your data from Facebook, to get files to work with. Just follow instructions from here [https://www.facebook.com/help/131112897028467/]. The only category you have to download is "Messages", you should choose a JSON file. Then unpack provided archive and point my program to message.json file of conversation you want to see stats about.

Example: (TODO :P)

Available options:

-a Print stats about authors (messages share by person).

-t Print stats about messages in general (total count of messages).

-w Print stats about words (most used ones).

It is completely correct to run program with all above options enabled at once.

Future plans:

Draw some fancy graphs.

Words stats for each person.
