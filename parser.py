"""Basic Facebook Messages Analyzer.
   d0ku 2018"""

import json
import operator
import argparse

# TODO: Convert input to appropriate encoding.
# TODO: Implement most common words by user.
# TODO: Implement proper media handling.


def parse_json_utf_8(text):
    return text


class Message():
    """Message class represents one message from conversation, and provides
    information about author, date, and content."""

    def __init__(self, author, timestamp, text):
        self.author = author
        self.timestamp = timestamp
        self.text = text
        self.words_stats = {}
        self.word_count = 0

        for word in text.split():
            self.word_count += 1
            try:
                self.words_stats[word] += 1
            except KeyError:
                self.words_stats[word] = 1

    def pretty(self):
        """Print message in human-readable from."""
        print(str(self.timestamp) + " " + self.author + " " + self.text)


class Parser:
    """Basic parser for message.json file downloaded from Facebook. It parses
    JSON file and provides some statistics about data."""

    def __init__(self, filename):
        self.parse_messages(filename)

    def parse_messages(self, filename):
        """Parse messages from provided file (JSON)."""
        with open(filename, "r", encoding="utf-8") as json_file:
            self.json_object = json.load(json_file)

        self.message_objects = []
        self.authors = {}
        self.messages_count = 0
        self.words_total_count = 0
        for message in self.json_object['messages']:
            self.messages_count += 1

            try:
                author = message['sender_name']
                try:
                    self.authors[author] += 1
                except KeyError:
                    self.authors[author] = 1
                timestamp = message['timestamp']
                text = message['content']
            except KeyError:   # picture or video
                continue

            message = Message(author, timestamp, text)
            self.words_total_count += message.word_count
            self.message_objects.append(message)

        self.sort_messages()

    def sort_messages(self):
        """Sort messages by attribute (date ascending by default)."""
        self.message_objects.sort(key=lambda obj: obj.timestamp, reverse=True)

    def print_all(self):
        """Print all messages one by one in human-readable form."""
        print("MESSAGES COUNT: "+str(len(self.message_objects)))
        for message in self.message_objects:
            message.pretty()
        print()

    def print_stats_words(self):
        """Print most common words in conversation."""
        temp_dict = {}

        for message in self.message_objects:
            for key in message.words_stats.keys():
                try:
                    temp_dict[key] += message.words_stats[key]
                except KeyError:
                    temp_dict[key] = message.words_stats[key]

        sorted_frequency_words = sorted(temp_dict.items(),
                                        key=operator.itemgetter(1),
                                        reverse=True)

        counter = 0
        print("TOTAL WORDS: "+str(self.words_total_count))

        for word in sorted_frequency_words:
            print(word[0] + " " + str(word[1]) + " -> " +
                  str(word[1]/self.words_total_count*100)+"%")
            counter += 1
            if counter == 10:
                break

        print()

    def print_stats_total(self):
        """Print statistics about conversation in total."""
        print("MESSAGES COUNT: " + str(self.messages_count))

    def print_stats_authors(self):
        """Print stats of individual users in conversation (message count, most
        common words) etc."""
        print("TOTAL MESSAGES: "+str(self.messages_count))
        for author in self.authors:
            print(author)
            print(str(self.authors[author]) + " -> " +
                  str(self.authors[author]/self.messages_count*100) + "%")
        print()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-f", "--file", help="JSON file with message data. "
                                             "It is usually 'message.json'",
                        required=True)

    parser.add_argument("-a", "--authors", help="Print stats about authors.",
                        action="store_true")
    parser.add_argument("-t", "--total", help="Print stats about messages "
                        "in general.", action="store_true")
    parser.add_argument("-w", "--words", help="Print stats about words "
                        "e.g. most common words.", action="store_true")

    result = parser.parse_args()

    parser = Parser(result.file)

    if result.words:
        parser.print_stats_words()
    if result.authors:
        parser.print_stats_authors()
    if result.total:
        parser.print_stats_total()
    # parser.print_all()
